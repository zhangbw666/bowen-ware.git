package io.bowen.common.validator;
import io.bowen.common.exception.BWException;
import org.apache.commons.lang.StringUtils;

/**
 * 数据校验
 */
public abstract class Assert {

    public static void isBlank(String str, String message) {
        if (StringUtils.isBlank(str)) {
            throw new BWException(message);
        }
    }

    public static void isNull(Object object, String message) {
        if (object == null) {
            throw new BWException(message);
        }
    }
}
