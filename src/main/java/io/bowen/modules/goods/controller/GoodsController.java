package io.bowen.modules.goods.controller;

import io.bowen.common.utils.PageUtils;
import io.bowen.common.utils.R;
import io.bowen.modules.goods.entity.GoodsEntity;
import io.bowen.modules.goods.service.GoodsService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;



/**
 * 商品管理
 *
 * @author zhangbowen
 * @email 478231309@qq.com
 * @date 2020-06-19 11:03:25
 */
@RestController
@RequestMapping("/go/goods")
public class GoodsController {
    @Autowired
    private GoodsService goodsService;

    /**
     * 列表
     */
    @RequestMapping("/list")
    @RequiresPermissions("go:goods:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = goodsService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @RequestMapping("/info/{goodsId}")
    @RequiresPermissions("go:goods:info")
    public R info(@PathVariable("goodsId") Long goodsId){
		GoodsEntity goods = goodsService.getById(goodsId);

        return R.ok().put("goods", goods);
    }

    /**
     * 保存
     */
    @RequestMapping("/save")
    @RequiresPermissions("go:goods:save")
    public R save(@RequestBody GoodsEntity goods){
		goodsService.save(goods);

        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    @RequiresPermissions("go:goods:update")
    public R update(@RequestBody GoodsEntity goods){
		goodsService.updateById(goods);

        return R.ok();
    }

    /**
     * 删除
     */
    @RequestMapping("/delete")
    @RequiresPermissions("go:goods:delete")
    public R delete(@RequestBody Long[] goodsIds){
		goodsService.removeByIds(Arrays.asList(goodsIds));

        return R.ok();
    }

}
