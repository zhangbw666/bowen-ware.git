@echo off
set port=8082
echo port : %port%

for /f "usebackq tokens=1-5" %%a in (`netstat -ano ^| findstr %port%`) do (
	if [%%d] EQU [LISTENING] (
		set pid=%%e
	)
)

for /f "usebackq tokens=1-5" %%a in (`tasklist ^| findstr %pid%`) do (
	set bowen_ware=%%a
)

echo now will kill process : pid %pid%, bowen_ware %bowen_ware%
pause
taskkill /f /pid %pid%
pause