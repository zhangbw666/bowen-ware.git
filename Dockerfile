FROM java:8
EXPOSE 8082

VOLUME /tmp
ADD bowen-ware.jar  /app.jar
RUN bash -c 'touch /app.jar'
ENTRYPOINT ["java","-jar","/app.jar"]
