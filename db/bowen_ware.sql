/*
Navicat MySQL Data Transfer

Source Server         : 本地环境-个人
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : bowen_ware

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2020-07-06 18:07:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `BLOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `SCHED_NAME` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `CALENDAR_NAME` varchar(200) NOT NULL,
  `CALENDAR` blob NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`CALENDAR_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `CRON_EXPRESSION` varchar(120) NOT NULL,
  `TIME_ZONE_ID` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', '0 0/30 * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `ENTRY_ID` varchar(95) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `FIRED_TIME` bigint(13) NOT NULL,
  `SCHED_TIME` bigint(13) NOT NULL,
  `PRIORITY` int(11) NOT NULL,
  `STATE` varchar(16) NOT NULL,
  `JOB_NAME` varchar(200) DEFAULT NULL,
  `JOB_GROUP` varchar(200) DEFAULT NULL,
  `IS_NONCONCURRENT` varchar(1) DEFAULT NULL,
  `REQUESTS_RECOVERY` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`ENTRY_ID`),
  KEY `IDX_QRTZ_FT_TRIG_INST_NAME` (`SCHED_NAME`,`INSTANCE_NAME`),
  KEY `IDX_QRTZ_FT_INST_JOB_REQ_RCVRY` (`SCHED_NAME`,`INSTANCE_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_FT_J_G` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_FT_T_G` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_FT_TG` (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `JOB_CLASS_NAME` varchar(250) NOT NULL,
  `IS_DURABLE` varchar(1) NOT NULL,
  `IS_NONCONCURRENT` varchar(1) NOT NULL,
  `IS_UPDATE_DATA` varchar(1) NOT NULL,
  `REQUESTS_RECOVERY` varchar(1) NOT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_J_REQ_RECOVERY` (`SCHED_NAME`,`REQUESTS_RECOVERY`),
  KEY `IDX_QRTZ_J_GRP` (`SCHED_NAME`,`JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', null, 'io.bowen.modules.job.utils.ScheduleJob', '0', '0', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002D696F2E626F77656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000172C1953E387874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740005626F77656E74000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000007800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `LOCK_NAME` varchar(40) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`LOCK_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RenrenScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `INSTANCE_NAME` varchar(200) NOT NULL,
  `LAST_CHECKIN_TIME` bigint(13) NOT NULL,
  `CHECKIN_INTERVAL` bigint(13) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`INSTANCE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RenrenScheduler', 'LAPTOP-TDMKHG5H1594020476218', '1594030021326', '15000');

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `REPEAT_COUNT` bigint(7) NOT NULL,
  `REPEAT_INTERVAL` bigint(12) NOT NULL,
  `TIMES_TRIGGERED` bigint(10) NOT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `STR_PROP_1` varchar(512) DEFAULT NULL,
  `STR_PROP_2` varchar(512) DEFAULT NULL,
  `STR_PROP_3` varchar(512) DEFAULT NULL,
  `INT_PROP_1` int(11) DEFAULT NULL,
  `INT_PROP_2` int(11) DEFAULT NULL,
  `LONG_PROP_1` bigint(20) DEFAULT NULL,
  `LONG_PROP_2` bigint(20) DEFAULT NULL,
  `DEC_PROP_1` decimal(13,4) DEFAULT NULL,
  `DEC_PROP_2` decimal(13,4) DEFAULT NULL,
  `BOOL_PROP_1` varchar(1) DEFAULT NULL,
  `BOOL_PROP_2` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`) REFERENCES `qrtz_triggers` (`SCHED_NAME`, `TRIGGER_NAME`, `TRIGGER_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `SCHED_NAME` varchar(120) NOT NULL,
  `TRIGGER_NAME` varchar(200) NOT NULL,
  `TRIGGER_GROUP` varchar(200) NOT NULL,
  `JOB_NAME` varchar(200) NOT NULL,
  `JOB_GROUP` varchar(200) NOT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `NEXT_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PREV_FIRE_TIME` bigint(13) DEFAULT NULL,
  `PRIORITY` int(11) DEFAULT NULL,
  `TRIGGER_STATE` varchar(16) NOT NULL,
  `TRIGGER_TYPE` varchar(8) NOT NULL,
  `START_TIME` bigint(13) NOT NULL,
  `END_TIME` bigint(13) DEFAULT NULL,
  `CALENDAR_NAME` varchar(200) DEFAULT NULL,
  `MISFIRE_INSTR` smallint(2) DEFAULT NULL,
  `JOB_DATA` blob,
  PRIMARY KEY (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_J` (`SCHED_NAME`,`JOB_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_JG` (`SCHED_NAME`,`JOB_GROUP`),
  KEY `IDX_QRTZ_T_C` (`SCHED_NAME`,`CALENDAR_NAME`),
  KEY `IDX_QRTZ_T_G` (`SCHED_NAME`,`TRIGGER_GROUP`),
  KEY `IDX_QRTZ_T_STATE` (`SCHED_NAME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_STATE` (`SCHED_NAME`,`TRIGGER_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_N_G_STATE` (`SCHED_NAME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NEXT_FIRE_TIME` (`SCHED_NAME`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST` (`SCHED_NAME`,`TRIGGER_STATE`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_STATE`),
  KEY `IDX_QRTZ_T_NFT_ST_MISFIRE_GRP` (`SCHED_NAME`,`MISFIRE_INSTR`,`NEXT_FIRE_TIME`,`TRIGGER_GROUP`,`TRIGGER_STATE`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`) REFERENCES `qrtz_job_details` (`SCHED_NAME`, `JOB_NAME`, `JOB_GROUP`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RenrenScheduler', 'TASK_1', 'DEFAULT', 'TASK_1', 'DEFAULT', null, '1592400600000', '-1', '5', 'PAUSED', 'CRON', '1592400001000', '0', null, '2', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000D4A4F425F504152414D5F4B45597372002D696F2E626F77656E2E6D6F64756C65732E6A6F622E656E746974792E5363686564756C654A6F62456E7469747900000000000000010200074C00086265616E4E616D657400124C6A6176612F6C616E672F537472696E673B4C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C000E63726F6E45787072657373696F6E71007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C0006706172616D7371007E00094C000672656D61726B71007E00094C00067374617475737400134C6A6176612F6C616E672F496E74656765723B7870740008746573745461736B7372000E6A6176612E7574696C2E44617465686A81014B5974190300007870770800000172C1953E387874000E3020302F3330202A202A202A203F7372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740005626F77656E74000CE58F82E695B0E6B58BE8AF95737200116A6176612E6C616E672E496E746567657212E2A0A4F781873802000149000576616C75657871007E0013000000017800);

-- ----------------------------
-- Table structure for schedule_job
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job`;
CREATE TABLE `schedule_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `cron_expression` varchar(100) DEFAULT NULL COMMENT 'cron表达式',
  `status` tinyint(4) DEFAULT NULL COMMENT '任务状态  0：正常  1：暂停',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='定时任务';

-- ----------------------------
-- Records of schedule_job
-- ----------------------------
INSERT INTO `schedule_job` VALUES ('1', 'testTask', 'bowen', '0 0/30 * * * ?', '1', '参数测试', '2020-06-17 17:21:23');

-- ----------------------------
-- Table structure for schedule_job_log
-- ----------------------------
DROP TABLE IF EXISTS `schedule_job_log`;
CREATE TABLE `schedule_job_log` (
  `log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志id',
  `job_id` bigint(20) NOT NULL COMMENT '任务id',
  `bean_name` varchar(200) DEFAULT NULL COMMENT 'spring bean名称',
  `params` varchar(2000) DEFAULT NULL COMMENT '参数',
  `status` tinyint(4) NOT NULL COMMENT '任务状态  0：成功  1：失败',
  `error` varchar(2000) DEFAULT NULL COMMENT '失败信息',
  `times` int(11) NOT NULL COMMENT '耗时(单位：毫秒)',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`log_id`),
  KEY `job_id` (`job_id`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8 COMMENT='定时任务日志';

-- ----------------------------
-- Records of schedule_job_log
-- ----------------------------
INSERT INTO `schedule_job_log` VALUES ('1', '1', 'testTask', 'bowen', '0', null, '5', '2020-06-17 21:30:00');
INSERT INTO `schedule_job_log` VALUES ('2', '1', 'testTask', 'bowen', '0', null, '2', '2020-06-17 22:00:01');
INSERT INTO `schedule_job_log` VALUES ('3', '1', 'testTask', 'bowen', '0', null, '2', '2020-06-17 22:30:00');
INSERT INTO `schedule_job_log` VALUES ('4', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-17 23:00:00');
INSERT INTO `schedule_job_log` VALUES ('5', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-17 23:30:00');
INSERT INTO `schedule_job_log` VALUES ('6', '1', 'testTask', 'bowen', '0', null, '6', '2020-06-18 00:00:00');
INSERT INTO `schedule_job_log` VALUES ('7', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 00:30:00');
INSERT INTO `schedule_job_log` VALUES ('8', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 01:00:00');
INSERT INTO `schedule_job_log` VALUES ('9', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 01:30:00');
INSERT INTO `schedule_job_log` VALUES ('10', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 02:00:00');
INSERT INTO `schedule_job_log` VALUES ('11', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 02:30:00');
INSERT INTO `schedule_job_log` VALUES ('12', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 03:00:00');
INSERT INTO `schedule_job_log` VALUES ('13', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 03:30:00');
INSERT INTO `schedule_job_log` VALUES ('14', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 04:00:00');
INSERT INTO `schedule_job_log` VALUES ('15', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 04:30:00');
INSERT INTO `schedule_job_log` VALUES ('16', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 05:00:00');
INSERT INTO `schedule_job_log` VALUES ('17', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 05:30:00');
INSERT INTO `schedule_job_log` VALUES ('18', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 06:00:00');
INSERT INTO `schedule_job_log` VALUES ('19', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 06:30:00');
INSERT INTO `schedule_job_log` VALUES ('20', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 07:00:00');
INSERT INTO `schedule_job_log` VALUES ('21', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 07:30:00');
INSERT INTO `schedule_job_log` VALUES ('22', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 08:00:00');
INSERT INTO `schedule_job_log` VALUES ('23', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 08:30:00');
INSERT INTO `schedule_job_log` VALUES ('24', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 09:00:00');
INSERT INTO `schedule_job_log` VALUES ('25', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 09:30:00');
INSERT INTO `schedule_job_log` VALUES ('26', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 10:00:00');
INSERT INTO `schedule_job_log` VALUES ('27', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('28', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('29', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('30', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('31', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('32', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('33', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('34', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-18 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('35', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 14:30:00');
INSERT INTO `schedule_job_log` VALUES ('36', '1', 'testTask', 'bowen', '0', null, '2', '2020-06-18 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('37', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('38', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('39', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-18 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('40', '1', 'testTask', 'bowen', '0', null, '19', '2020-06-18 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('41', '1', 'testTask', 'bowen', '0', null, '2', '2020-06-19 10:30:00');
INSERT INTO `schedule_job_log` VALUES ('42', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-19 11:00:00');
INSERT INTO `schedule_job_log` VALUES ('43', '1', 'testTask', 'bowen', '0', null, '2', '2020-06-19 11:30:00');
INSERT INTO `schedule_job_log` VALUES ('44', '1', 'testTask', 'bowen', '0', null, '2', '2020-06-19 12:00:00');
INSERT INTO `schedule_job_log` VALUES ('45', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-19 12:30:00');
INSERT INTO `schedule_job_log` VALUES ('46', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-19 13:00:00');
INSERT INTO `schedule_job_log` VALUES ('47', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-19 13:30:00');
INSERT INTO `schedule_job_log` VALUES ('48', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-19 14:00:00');
INSERT INTO `schedule_job_log` VALUES ('49', '1', 'testTask', 'bowen', '0', null, '3', '2020-06-19 15:00:00');
INSERT INTO `schedule_job_log` VALUES ('50', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-19 15:30:00');
INSERT INTO `schedule_job_log` VALUES ('51', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-19 16:00:00');
INSERT INTO `schedule_job_log` VALUES ('52', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-19 16:30:00');
INSERT INTO `schedule_job_log` VALUES ('53', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-19 17:00:00');
INSERT INTO `schedule_job_log` VALUES ('54', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-19 17:30:00');
INSERT INTO `schedule_job_log` VALUES ('55', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-24 11:18:04');
INSERT INTO `schedule_job_log` VALUES ('56', '1', 'testTask', 'bowen', '0', null, '6034', '2020-06-24 11:21:03');
INSERT INTO `schedule_job_log` VALUES ('57', '1', 'testTask', 'bowen', '0', null, '4102', '2020-06-24 11:33:44');
INSERT INTO `schedule_job_log` VALUES ('58', '1', 'testTask', 'bowen', '0', null, '0', '2020-06-24 11:34:01');
INSERT INTO `schedule_job_log` VALUES ('59', '1', 'testTask', 'bowen', '0', null, '1', '2020-06-24 11:34:01');

-- ----------------------------
-- Table structure for sys_captcha
-- ----------------------------
DROP TABLE IF EXISTS `sys_captcha`;
CREATE TABLE `sys_captcha` (
  `uuid` char(36) NOT NULL COMMENT 'uuid',
  `code` varchar(6) NOT NULL COMMENT '验证码',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  PRIMARY KEY (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统验证码';

-- ----------------------------
-- Records of sys_captcha
-- ----------------------------
INSERT INTO `sys_captcha` VALUES ('0001f764-7e91-449e-8108-231a580a72f2', '4bbme', '2020-06-17 21:58:33');
INSERT INTO `sys_captcha` VALUES ('111', '73apd', '2020-07-02 15:38:02');
INSERT INTO `sys_captcha` VALUES ('1a454920-54df-4dea-8efd-ca09220baa11', 'mf77p', '2020-06-30 19:16:25');
INSERT INTO `sys_captcha` VALUES ('30a259b2-fc52-4fca-8a8f-7a0abbf2c684', 'ex2dn', '2020-06-17 21:57:45');
INSERT INTO `sys_captcha` VALUES ('31014fe4-58b7-4021-8a4c-f493f87da93f', 'pn2fm', '2020-07-01 10:55:58');
INSERT INTO `sys_captcha` VALUES ('31439b6a-6185-4137-86f0-9ac09e5a73d4', 'baeed', '2020-06-23 15:04:37');
INSERT INTO `sys_captcha` VALUES ('4602e560-3300-4411-87a2-10266d30edfe', 'g5nfe', '2020-06-19 17:53:40');
INSERT INTO `sys_captcha` VALUES ('5204bba2-bec5-4ea8-8806-9bf6c8a5c602', '65xyf', '2020-07-06 16:53:59');
INSERT INTO `sys_captcha` VALUES ('5fbb0712-2b4c-4135-85d2-21b17692e5cb', 'c8epe', '2020-06-17 22:15:22');
INSERT INTO `sys_captcha` VALUES ('60029c07-0e42-437c-8860-a231a6057b84', 'xcaba', '2020-06-17 21:58:10');
INSERT INTO `sys_captcha` VALUES ('67e954d6-ea31-48c0-8572-9c6865349644', '7ed42', '2020-06-17 22:10:32');
INSERT INTO `sys_captcha` VALUES ('6f6e06cd-a7b9-4e0b-882b-36a1090d8cd8', 'wfenf', '2020-06-30 11:30:55');
INSERT INTO `sys_captcha` VALUES ('70bf42d3-ee58-4f78-8ece-fd40e1482356', 'fpmay', '2020-06-19 11:25:11');
INSERT INTO `sys_captcha` VALUES ('73ed0fd8-a14e-43e5-87c8-bde928dc882c', 'my64a', '2020-07-01 10:55:50');
INSERT INTO `sys_captcha` VALUES ('7f54d5ee-0580-483c-8160-7cfe5e8c7d07', 'nc3m4', '2020-06-30 15:45:29');
INSERT INTO `sys_captcha` VALUES ('9671c11b-f989-4324-8d53-ac3db2a63b6b', 'ec8fn', '2020-06-17 22:25:01');
INSERT INTO `sys_captcha` VALUES ('9690ab54-8364-475f-89a5-9d05fabe1b54', 'aa847', '2020-06-18 16:22:16');
INSERT INTO `sys_captcha` VALUES ('98ca1bc7-ca84-4084-89f5-7fe6bf976e8c', 'ynpn3', '2020-06-17 22:12:52');
INSERT INTO `sys_captcha` VALUES ('a102c61e-1ef0-40b3-8997-25f971425f69', 'anynn', '2020-06-18 16:12:54');
INSERT INTO `sys_captcha` VALUES ('b66f9e18-abeb-4866-8a2a-7692cd0d583b', 'c8gg6', '2020-06-30 15:55:19');
INSERT INTO `sys_captcha` VALUES ('c65dd649-cf6f-4c5a-81c4-080987fb8445', 'xdcc2', '2020-06-30 18:00:56');
INSERT INTO `sys_captcha` VALUES ('dc03bc08-5271-48f6-81d6-46668ba93573', 'fecdf', '2020-06-18 15:51:22');
INSERT INTO `sys_captcha` VALUES ('de03f445-a23d-40f8-803e-e7d4899f99f5', '4wyap', '2020-06-17 22:24:55');
INSERT INTO `sys_captcha` VALUES ('ee69c30c-60df-43c1-8ab5-31d21f639068', 'cwdna', '2020-06-17 21:59:13');
INSERT INTO `sys_captcha` VALUES ('ef1df944-fcf6-4a26-8869-68de5c915858', 'wxwce', '2020-06-19 11:41:54');
INSERT INTO `sys_captcha` VALUES ('fee5e008-b661-4629-8bfc-2a680685fdbd', '66pma', '2020-06-23 15:20:03');
INSERT INTO `sys_captcha` VALUES ('fefee27e-2f1b-476a-8f32-2564c8ebf563', 'fy52n', '2020-06-17 22:24:55');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `param_key` varchar(50) DEFAULT NULL COMMENT 'key',
  `param_value` varchar(2000) DEFAULT NULL COMMENT 'value',
  `status` tinyint(4) DEFAULT '1' COMMENT '状态   0：隐藏   1：显示',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `param_key` (`param_key`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='系统配置信息表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', 'CLOUD_STORAGE_CONFIG_KEY', '{\"type\":1,\"qiniuDomain\":\"http://7xqbwh.dl1.z0.glb.clouddn.com\",\"qiniuPrefix\":\"upload\",\"qiniuAccessKey\":\"NrgMfABZxWLo5B-YYSjoE8-AZ1EISdi1Z3ubLOeZ\",\"qiniuSecretKey\":\"uIwJHevMRWU0VLxFvgy0tAcOdGqasdtVlJkdy6vV\",\"qiniuBucketName\":\"ios-app\",\"aliyunDomain\":\"\",\"aliyunPrefix\":\"\",\"aliyunEndPoint\":\"\",\"aliyunAccessKeyId\":\"\",\"aliyunAccessKeySecret\":\"\",\"aliyunBucketName\":\"\",\"qcloudDomain\":\"\",\"qcloudPrefix\":\"\",\"qcloudSecretId\":\"\",\"qcloudSecretKey\":\"\",\"qcloudBucketName\":\"\"}', '0', '云存储配置信息');
INSERT INTO `sys_config` VALUES ('2', 'ORDER', 'U01010012', '1', '订单参数');
INSERT INTO `sys_config` VALUES ('3', 'MANAGER', '200000101012', '1', '鞋类参数');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL COMMENT '用户名',
  `operation` varchar(50) DEFAULT NULL COMMENT '用户操作',
  `method` varchar(200) DEFAULT NULL COMMENT '请求方法',
  `params` varchar(5000) DEFAULT NULL COMMENT '请求参数',
  `time` bigint(20) NOT NULL COMMENT '执行时长(毫秒)',
  `ip` varchar(64) DEFAULT NULL COMMENT 'IP地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COMMENT='系统日志';

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES ('1', 'admin', '修改密码', 'io.bowen.modules.sys.controller.SysUserController.password()', '[{\"password\":\"123456\",\"newPassword\":\"123456\"}]', '26', '0:0:0:0:0:0:0:1', '2020-06-18 16:25:16');
INSERT INTO `sys_log` VALUES ('2', 'admin', '修改定时任务', 'io.bowen.modules.job.controller.ScheduleJobController.update()', '[{\"jobId\":1,\"beanName\":\"testTask\",\"params\":\"bowen\",\"cronExpression\":\"0 0/30 * * * ?\",\"status\":0,\"remark\":\"参数测试\"}]', '246', '0:0:0:0:0:0:0:1', '2020-06-18 16:28:29');
INSERT INTO `sys_log` VALUES ('3', 'admin', '修改用户', 'io.bowen.modules.sys.controller.SysUserController.update()', '[{\"userId\":1,\"username\":\"admin\",\"password\":\"cdac762d0ba79875489f6a8b430fa8b5dfe0cdd81da38b80f02f33328af7fd4a\",\"salt\":\"YzcmCZNvbXocrsz9dm8e\",\"email\":\"478231309@qq.com\",\"mobile\":\"15766190085\",\"status\":1,\"roleIdList\":[],\"createUserId\":1}]', '328', '0:0:0:0:0:0:0:1', '2020-06-19 17:17:10');
INSERT INTO `sys_log` VALUES ('4', 'admin', '删除用户', 'io.bowen.modules.sys.controller.SysUserController.delete()', '[[1]]', '5', '0:0:0:0:0:0:0:1', '2020-06-19 17:17:26');
INSERT INTO `sys_log` VALUES ('5', 'admin', '保存角色', 'io.bowen.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":1,\"roleName\":\"商品管理角色\",\"remark\":\"只能管理商品模块\",\"createUserId\":1,\"menuIdList\":[31,32,33,34,35,-666666,1],\"createTime\":\"Jun 19, 2020 5:20:21 PM\"}]', '182', '0:0:0:0:0:0:0:1', '2020-06-19 17:20:21');
INSERT INTO `sys_log` VALUES ('6', 'admin', '保存菜单', 'io.bowen.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":36,\"parentId\":0,\"name\":\"订单管理\",\"type\":0,\"icon\":\"zonghe\",\"orderNum\":0}]', '7', '0:0:0:0:0:0:0:1', '2020-06-19 17:22:01');
INSERT INTO `sys_log` VALUES ('7', 'admin', '保存菜单', 'io.bowen.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":37,\"parentId\":36,\"name\":\"订单查询\",\"url\":\"/order/list\",\"type\":1,\"icon\":\"job\",\"orderNum\":0}]', '12', '0:0:0:0:0:0:0:1', '2020-06-19 17:24:17');
INSERT INTO `sys_log` VALUES ('8', 'admin', '保存菜单', 'io.bowen.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":38,\"parentId\":36,\"name\":\"订单新增\",\"url\":\"/order/add\",\"type\":1,\"icon\":\"editor\",\"orderNum\":0}]', '5', '0:0:0:0:0:0:0:1', '2020-06-19 17:25:17');
INSERT INTO `sys_log` VALUES ('9', 'admin', '修改菜单', 'io.bowen.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":36,\"parentId\":0,\"name\":\"订单管理\",\"type\":0,\"icon\":\"zonghe\",\"orderNum\":1}]', '16', '0:0:0:0:0:0:0:1', '2020-06-19 17:25:35');
INSERT INTO `sys_log` VALUES ('10', 'admin', '修改菜单', 'io.bowen.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":1,\"parentId\":0,\"name\":\"系统管理\",\"type\":0,\"icon\":\"system\",\"orderNum\":2}]', '6', '0:0:0:0:0:0:0:1', '2020-06-19 17:25:43');
INSERT INTO `sys_log` VALUES ('11', 'admin', '修改菜单', 'io.bowen.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":37,\"parentId\":36,\"name\":\"订单查询\",\"url\":\"/order/list\",\"type\":1,\"icon\":\"job\",\"orderNum\":1}]', '13', '0:0:0:0:0:0:0:1', '2020-06-19 17:26:42');
INSERT INTO `sys_log` VALUES ('12', 'admin', '修改菜单', 'io.bowen.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":38,\"parentId\":36,\"name\":\"订单新增\",\"url\":\"/order/add\",\"type\":1,\"icon\":\"editor\",\"orderNum\":2}]', '5', '0:0:0:0:0:0:0:1', '2020-06-19 17:26:53');
INSERT INTO `sys_log` VALUES ('13', 'admin', '修改菜单', 'io.bowen.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":30,\"parentId\":1,\"name\":\"文件上传\",\"url\":\"oss/oss\",\"perms\":\"sys:oss:all\",\"type\":1,\"icon\":\"oss\",\"orderNum\":7}]', '8', '0:0:0:0:0:0:0:1', '2020-06-19 17:27:36');
INSERT INTO `sys_log` VALUES ('14', 'admin', '修改菜单', 'io.bowen.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":30,\"parentId\":1,\"name\":\"文件上传\",\"url\":\"oss/oss\",\"perms\":\"sys:oss:all\",\"type\":1,\"icon\":\"oss\",\"orderNum\":8}]', '6', '0:0:0:0:0:0:0:1', '2020-06-19 17:27:49');
INSERT INTO `sys_log` VALUES ('15', 'admin', '修改菜单', 'io.bowen.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":31,\"parentId\":1,\"name\":\"商品管理\",\"url\":\"go/goods\",\"type\":1,\"icon\":\"config\",\"orderNum\":9}]', '15', '0:0:0:0:0:0:0:1', '2020-06-19 17:28:00');
INSERT INTO `sys_log` VALUES ('16', 'admin', '保存菜单', 'io.bowen.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":39,\"parentId\":37,\"name\":\"查询\",\"url\":\"\",\"perms\":\"\",\"type\":2,\"icon\":\"\",\"orderNum\":0}]', '12', '0:0:0:0:0:0:0:1', '2020-06-19 17:29:05');
INSERT INTO `sys_log` VALUES ('17', 'admin', '保存菜单', 'io.bowen.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":40,\"parentId\":38,\"name\":\"新增\",\"url\":\"\",\"perms\":\"\",\"type\":2,\"icon\":\"\",\"orderNum\":0}]', '13', '0:0:0:0:0:0:0:1', '2020-06-19 17:29:29');
INSERT INTO `sys_log` VALUES ('18', 'admin', '保存菜单', 'io.bowen.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":41,\"parentId\":38,\"name\":\"重置\",\"url\":\"\",\"perms\":\"\",\"type\":2,\"icon\":\"\",\"orderNum\":0}]', '14', '0:0:0:0:0:0:0:1', '2020-06-19 17:30:06');
INSERT INTO `sys_log` VALUES ('19', 'admin', '保存菜单', 'io.bowen.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":42,\"parentId\":37,\"name\":\"重置\",\"url\":\"\",\"perms\":\"\",\"type\":2,\"icon\":\"\",\"orderNum\":0}]', '12', '0:0:0:0:0:0:0:1', '2020-06-19 17:30:17');
INSERT INTO `sys_log` VALUES ('20', 'admin', '修改角色', 'io.bowen.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":1,\"roleName\":\"商品管理角色\",\"remark\":\"只能管理商品模块\",\"createUserId\":1,\"menuIdList\":[31,32,33,34,35,36,37,39,42,38,40,41,-666666,1]}]', '45', '0:0:0:0:0:0:0:1', '2020-06-19 17:33:47');
INSERT INTO `sys_log` VALUES ('21', 'admin', '修改角色', 'io.bowen.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":1,\"roleName\":\"O2O角色\",\"remark\":\"订单、商品管理\",\"createUserId\":1,\"menuIdList\":[31,32,33,34,35,36,37,39,42,38,40,41,-666666,1]}]', '27', '0:0:0:0:0:0:0:1', '2020-06-19 17:34:20');
INSERT INTO `sys_log` VALUES ('22', 'admin', '暂停定时任务', 'io.bowen.modules.job.controller.ScheduleJobController.pause()', '[[1]]', '38', '0:0:0:0:0:0:0:1', '2020-06-19 17:35:16');
INSERT INTO `sys_log` VALUES ('23', 'admin', '修改定时任务', 'io.bowen.modules.job.controller.ScheduleJobController.update()', '[{\"jobId\":1,\"beanName\":\"testTask\",\"params\":\"bowen\",\"cronExpression\":\"0 0/30 * * * ?\",\"status\":1,\"remark\":\"参数测试\"}]', '55', '0:0:0:0:0:0:0:1', '2020-06-19 17:35:48');
INSERT INTO `sys_log` VALUES ('24', 'admin', '保存配置', 'io.bowen.modules.sys.controller.SysConfigController.save()', '[{\"id\":2,\"paramKey\":\"ORDER\",\"paramValue\":\"1\",\"remark\":\"订单参数\"}]', '26', '0:0:0:0:0:0:0:1', '2020-06-19 17:36:46');
INSERT INTO `sys_log` VALUES ('25', 'admin', '保存配置', 'io.bowen.modules.sys.controller.SysConfigController.save()', '[{\"id\":3,\"paramKey\":\"manager\",\"paramValue\":\"2\",\"remark\":\"鞋类参数\"}]', '5', '0:0:0:0:0:0:0:1', '2020-06-19 17:37:06');
INSERT INTO `sys_log` VALUES ('26', 'admin', '修改配置', 'io.bowen.modules.sys.controller.SysConfigController.update()', '[{\"id\":3,\"paramKey\":\"MANAGER\",\"paramValue\":\"2\",\"remark\":\"鞋类参数\"}]', '8', '0:0:0:0:0:0:0:1', '2020-06-19 17:37:19');
INSERT INTO `sys_log` VALUES ('27', 'admin', '修改配置', 'io.bowen.modules.sys.controller.SysConfigController.update()', '[{\"id\":3,\"paramKey\":\"MANAGER\",\"paramValue\":\"200000101012\",\"remark\":\"鞋类参数\"}]', '13', '0:0:0:0:0:0:0:1', '2020-06-19 17:37:41');
INSERT INTO `sys_log` VALUES ('28', 'admin', '修改配置', 'io.bowen.modules.sys.controller.SysConfigController.update()', '[{\"id\":2,\"paramKey\":\"ORDER\",\"paramValue\":\"U01010012\",\"remark\":\"订单参数\"}]', '14', '0:0:0:0:0:0:0:1', '2020-06-19 17:37:56');
INSERT INTO `sys_log` VALUES ('29', 'admin', '保存用户', 'io.bowen.modules.sys.controller.SysUserController.save()', '[{\"userId\":2,\"username\":\"zbw\",\"password\":\"b55245d8c8d963396b687bbf24e12cc3bc4bdbce13b4dc13fa54c7b0cd3d73e0\",\"salt\":\"eZMZVqWczL1TrNkuIVjN\",\"email\":\"123@qq.com\",\"mobile\":\"15766190085\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1,\"createTime\":\"Jun 19, 2020 5:39:22 PM\"}]', '49', '0:0:0:0:0:0:0:1', '2020-06-19 17:39:22');
INSERT INTO `sys_log` VALUES ('30', 'admin', '保存角色', 'io.bowen.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":2,\"roleName\":\"角色管理员\",\"remark\":\"\",\"createUserId\":1,\"menuIdList\":[3,19,20,21,22,-666666,1],\"createTime\":\"Jun 19, 2020 5:41:58 PM\"}]', '17', '0:0:0:0:0:0:0:1', '2020-06-19 17:41:59');
INSERT INTO `sys_log` VALUES ('31', 'admin', '保存角色', 'io.bowen.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":3,\"roleName\":\"数据库管理员\",\"remark\":\"管理日志和数据库监控\",\"createUserId\":1,\"menuIdList\":[5,29,-666666,1],\"createTime\":\"Jun 19, 2020 5:42:37 PM\"}]', '13', '0:0:0:0:0:0:0:1', '2020-06-19 17:42:37');
INSERT INTO `sys_log` VALUES ('32', 'admin', '修改角色', 'io.bowen.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":2,\"roleName\":\"角色管理员\",\"remark\":\"负责创建角色\",\"createUserId\":1,\"menuIdList\":[3,19,20,21,22,-666666,1]}]', '14', '0:0:0:0:0:0:0:1', '2020-06-19 17:42:59');
INSERT INTO `sys_log` VALUES ('33', 'admin', '修改用户', 'io.bowen.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"zbw\",\"salt\":\"eZMZVqWczL1TrNkuIVjN\",\"email\":\"123@qq.com\",\"mobile\":\"15766190085\",\"status\":1,\"roleIdList\":[1,2],\"createUserId\":1}]', '17', '0:0:0:0:0:0:0:1', '2020-06-19 17:45:03');
INSERT INTO `sys_log` VALUES ('34', 'admin', '修改用户', 'io.bowen.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"role\",\"salt\":\"eZMZVqWczL1TrNkuIVjN\",\"email\":\"123@qq.com\",\"mobile\":\"15766190085\",\"status\":1,\"roleIdList\":[1,2],\"createUserId\":1}]', '10', '0:0:0:0:0:0:0:1', '2020-06-19 17:45:17');
INSERT INTO `sys_log` VALUES ('35', 'admin', '保存用户', 'io.bowen.modules.sys.controller.SysUserController.save()', '[{\"userId\":3,\"username\":\"dba\",\"password\":\"0eb94e42b153fdb11db3f59c6c80aa952b3a8ccfafb0c43eb0f7b1ad3ab95d63\",\"salt\":\"NI3V7I3bx13Yk0wmwFCM\",\"email\":\"123@qq.com\",\"mobile\":\"15766190085\",\"status\":1,\"roleIdList\":[3],\"createUserId\":1,\"createTime\":\"Jun 19, 2020 5:46:04 PM\"}]', '11', '0:0:0:0:0:0:0:1', '2020-06-19 17:46:05');
INSERT INTO `sys_log` VALUES ('36', 'admin', '保存用户', 'io.bowen.modules.sys.controller.SysUserController.save()', '[{\"userId\":4,\"username\":\"o2o\",\"password\":\"82d7e6146306f36fdd23e2a919eb7c5fb44950bf7403f4bf475f0fdb11708ebc\",\"salt\":\"qqTAPf3hhdi2AstrPzop\",\"email\":\"123@qq.com\",\"mobile\":\"15766190085\",\"status\":1,\"roleIdList\":[1],\"createUserId\":1,\"createTime\":\"Jun 19, 2020 5:47:12 PM\"}]', '17', '0:0:0:0:0:0:0:1', '2020-06-19 17:47:12');
INSERT INTO `sys_log` VALUES ('37', 'admin', '修改角色', 'io.bowen.modules.sys.controller.SysRoleController.update()', '[{\"roleId\":1,\"roleName\":\"O2O角色\",\"remark\":\"订单、商品管理\",\"createUserId\":1,\"menuIdList\":[31,32,33,34,35,36,37,39,42,38,40,41,-666666,1]}]', '28', '0:0:0:0:0:0:0:1', '2020-06-19 17:49:17');
INSERT INTO `sys_log` VALUES ('38', 'admin', '修改用户', 'io.bowen.modules.sys.controller.SysUserController.update()', '[{\"userId\":2,\"username\":\"role\",\"salt\":\"eZMZVqWczL1TrNkuIVjN\",\"email\":\"123@qq.com\",\"mobile\":\"15766190085\",\"status\":1,\"roleIdList\":[2],\"createUserId\":1}]', '13', '0:0:0:0:0:0:0:1', '2020-06-19 17:50:20');
INSERT INTO `sys_log` VALUES ('39', 'admin', '保存角色', 'io.bowen.modules.sys.controller.SysRoleController.save()', '[{\"roleId\":4,\"roleName\":\"开发角色\",\"remark\":\"开发人员使用\",\"createUserId\":1,\"menuIdList\":[1,2,15,16,17,18,3,19,20,21,22,4,23,24,25,26,5,6,7,8,9,10,11,12,13,14,27,29,30,31,32,33,34,35,36,37,39,42,38,40,41,-666666],\"createTime\":\"Jun 24, 2020 10:55:21 AM\"}]', '12477', '0:0:0:0:0:0:0:1', '2020-06-24 10:55:29');
INSERT INTO `sys_log` VALUES ('40', 'admin', '立即执行任务', 'io.bowen.modules.job.controller.ScheduleJobController.run()', '[[1]]', '9357', '0:0:0:0:0:0:0:1', '2020-06-24 11:18:04');
INSERT INTO `sys_log` VALUES ('41', 'admin', '立即执行任务', 'io.bowen.modules.job.controller.ScheduleJobController.run()', '[[1]]', '23', '0:0:0:0:0:0:0:1', '2020-06-24 11:21:03');
INSERT INTO `sys_log` VALUES ('42', 'admin', '修改定时任务', 'io.bowen.modules.job.controller.ScheduleJobController.update()', '[{\"jobId\":1,\"beanName\":\"testTask\",\"params\":\"bowen\",\"cronExpression\":\"0 0/30 * * * ?\",\"status\":1,\"remark\":\"参数测试\"}]', '56', '0:0:0:0:0:0:0:1', '2020-06-24 11:26:00');
INSERT INTO `sys_log` VALUES ('43', 'admin', '修改定时任务', 'io.bowen.modules.job.controller.ScheduleJobController.update()', '[{\"jobId\":1,\"beanName\":\"testTask\",\"params\":\"bowen\",\"cronExpression\":\"0 0/30 * * * ?\",\"status\":1,\"remark\":\"参数测试\"}]', '16157', '0:0:0:0:0:0:0:1', '2020-06-24 11:28:27');
INSERT INTO `sys_log` VALUES ('44', 'admin', '恢复定时任务', 'io.bowen.modules.job.controller.ScheduleJobController.resume()', '[[1]]', '26', '0:0:0:0:0:0:0:1', '2020-06-24 11:33:23');
INSERT INTO `sys_log` VALUES ('45', 'admin', '立即执行任务', 'io.bowen.modules.job.controller.ScheduleJobController.run()', '[[1]]', '34', '0:0:0:0:0:0:0:1', '2020-06-24 11:33:44');
INSERT INTO `sys_log` VALUES ('46', 'admin', '立即执行任务', 'io.bowen.modules.job.controller.ScheduleJobController.run()', '[[1]]', '26', '0:0:0:0:0:0:0:1', '2020-06-24 11:33:52');
INSERT INTO `sys_log` VALUES ('47', 'admin', '立即执行任务', 'io.bowen.modules.job.controller.ScheduleJobController.run()', '[[1]]', '34', '0:0:0:0:0:0:0:1', '2020-06-24 11:34:01');
INSERT INTO `sys_log` VALUES ('48', 'admin', '暂停定时任务', 'io.bowen.modules.job.controller.ScheduleJobController.pause()', '[[1]]', '17', '0:0:0:0:0:0:0:1', '2020-06-24 11:34:42');
INSERT INTO `sys_log` VALUES ('49', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[36]', '1', '127.0.0.1', '2020-07-06 16:01:44');
INSERT INTO `sys_log` VALUES ('50', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[37]', '2', '127.0.0.1', '2020-07-06 16:01:51');
INSERT INTO `sys_log` VALUES ('51', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[39]', '43', '127.0.0.1', '2020-07-06 16:02:00');
INSERT INTO `sys_log` VALUES ('52', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[42]', '15', '127.0.0.1', '2020-07-06 16:02:12');
INSERT INTO `sys_log` VALUES ('53', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[40]', '15', '127.0.0.1', '2020-07-06 16:02:20');
INSERT INTO `sys_log` VALUES ('54', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[41]', '15', '127.0.0.1', '2020-07-06 16:02:29');
INSERT INTO `sys_log` VALUES ('55', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[37]', '8', '127.0.0.1', '2020-07-06 16:02:41');
INSERT INTO `sys_log` VALUES ('56', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[38]', '14', '127.0.0.1', '2020-07-06 16:02:48');
INSERT INTO `sys_log` VALUES ('57', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[36]', '14', '127.0.0.1', '2020-07-06 16:02:53');
INSERT INTO `sys_log` VALUES ('58', 'admin', '修改菜单', 'io.bowen.modules.sys.controller.SysMenuController.update()', '[{\"menuId\":1,\"parentId\":0,\"name\":\"系统管理\",\"type\":0,\"icon\":\"system\",\"orderNum\":1}]', '8', '127.0.0.1', '2020-07-06 16:03:13');
INSERT INTO `sys_log` VALUES ('59', 'admin', '删除角色', 'io.bowen.modules.sys.controller.SysRoleController.delete()', '[[4]]', '27', '127.0.0.1', '2020-07-06 16:03:58');
INSERT INTO `sys_log` VALUES ('60', 'admin', '删除角色', 'io.bowen.modules.sys.controller.SysRoleController.delete()', '[[3]]', '5', '127.0.0.1', '2020-07-06 16:04:02');
INSERT INTO `sys_log` VALUES ('61', 'admin', '删除角色', 'io.bowen.modules.sys.controller.SysRoleController.delete()', '[[2]]', '15', '127.0.0.1', '2020-07-06 16:04:05');
INSERT INTO `sys_log` VALUES ('62', 'admin', '删除角色', 'io.bowen.modules.sys.controller.SysRoleController.delete()', '[[1]]', '13', '127.0.0.1', '2020-07-06 16:04:11');
INSERT INTO `sys_log` VALUES ('63', 'admin', '删除用户', 'io.bowen.modules.sys.controller.SysUserController.delete()', '[[4]]', '8', '127.0.0.1', '2020-07-06 16:04:20');
INSERT INTO `sys_log` VALUES ('64', 'admin', '删除用户', 'io.bowen.modules.sys.controller.SysUserController.delete()', '[[3]]', '4', '127.0.0.1', '2020-07-06 16:04:25');
INSERT INTO `sys_log` VALUES ('65', 'admin', '删除用户', 'io.bowen.modules.sys.controller.SysUserController.delete()', '[[2]]', '11', '127.0.0.1', '2020-07-06 16:04:28');
INSERT INTO `sys_log` VALUES ('66', 'admin', '删除用户', 'io.bowen.modules.sys.controller.SysUserController.delete()', '[[1]]', '0', '127.0.0.1', '2020-07-06 16:04:38');
INSERT INTO `sys_log` VALUES ('67', 'admin', '保存菜单', 'io.bowen.modules.sys.controller.SysMenuController.save()', '[{\"menuId\":43,\"parentId\":0,\"name\":\"商品管理\",\"url\":\"\",\"perms\":\"\",\"type\":0,\"icon\":\"zonghe\",\"orderNum\":2}]', '3', '127.0.0.1', '2020-07-06 16:15:19');
INSERT INTO `sys_log` VALUES ('68', 'admin', '删除菜单', 'io.bowen.modules.sys.controller.SysMenuController.delete()', '[43]', '12', '127.0.0.1', '2020-07-06 16:17:22');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父菜单ID，一级菜单为0',
  `name` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `url` varchar(200) DEFAULT NULL COMMENT '菜单URL',
  `perms` varchar(500) DEFAULT NULL COMMENT '授权(多个用逗号分隔，如：user:list,user:create)',
  `type` int(11) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `order_num` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8 COMMENT='菜单管理';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '0', '系统管理', null, null, '0', 'system', '1');
INSERT INTO `sys_menu` VALUES ('2', '1', '管理员列表', 'sys/user', null, '1', 'admin', '1');
INSERT INTO `sys_menu` VALUES ('3', '1', '角色管理', 'sys/role', null, '1', 'role', '2');
INSERT INTO `sys_menu` VALUES ('4', '1', '菜单管理', 'sys/menu', null, '1', 'menu', '3');
INSERT INTO `sys_menu` VALUES ('5', '1', 'SQL监控', 'http://localhost:8082/bowen-ware/druid/sql.html', null, '1', 'sql', '4');
INSERT INTO `sys_menu` VALUES ('6', '1', '定时任务', 'job/schedule', null, '1', 'job', '5');
INSERT INTO `sys_menu` VALUES ('7', '6', '查看', null, 'sys:schedule:list,sys:schedule:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('8', '6', '新增', null, 'sys:schedule:save', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('9', '6', '修改', null, 'sys:schedule:update', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('10', '6', '删除', null, 'sys:schedule:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('11', '6', '暂停', null, 'sys:schedule:pause', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('12', '6', '恢复', null, 'sys:schedule:resume', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('13', '6', '立即执行', null, 'sys:schedule:run', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('14', '6', '日志列表', null, 'sys:schedule:log', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('15', '2', '查看', null, 'sys:user:list,sys:user:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('16', '2', '新增', null, 'sys:user:save,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('17', '2', '修改', null, 'sys:user:update,sys:role:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('18', '2', '删除', null, 'sys:user:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('19', '3', '查看', null, 'sys:role:list,sys:role:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('20', '3', '新增', null, 'sys:role:save,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('21', '3', '修改', null, 'sys:role:update,sys:menu:list', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('22', '3', '删除', null, 'sys:role:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('23', '4', '查看', null, 'sys:menu:list,sys:menu:info', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('24', '4', '新增', null, 'sys:menu:save,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('25', '4', '修改', null, 'sys:menu:update,sys:menu:select', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('26', '4', '删除', null, 'sys:menu:delete', '2', null, '0');
INSERT INTO `sys_menu` VALUES ('27', '1', '参数管理', 'sys/config', 'sys:config:list,sys:config:info,sys:config:save,sys:config:update,sys:config:delete', '1', 'config', '6');
INSERT INTO `sys_menu` VALUES ('29', '1', '系统日志', 'sys/log', 'sys:log:list', '1', 'log', '7');
INSERT INTO `sys_menu` VALUES ('30', '1', '文件上传', 'oss/oss', 'sys:oss:all', '1', 'oss', '8');
INSERT INTO `sys_menu` VALUES ('31', '1', '商品管理', 'go/goods', null, '1', 'config', '9');
INSERT INTO `sys_menu` VALUES ('32', '31', '查看', null, 'go:goods:list,go:goods:info', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('33', '31', '新增', null, 'go:goods:save', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('34', '31', '修改', null, 'go:goods:update', '2', null, '6');
INSERT INTO `sys_menu` VALUES ('35', '31', '删除', null, 'go:goods:delete', '2', null, '6');

-- ----------------------------
-- Table structure for sys_oss
-- ----------------------------
DROP TABLE IF EXISTS `sys_oss`;
CREATE TABLE `sys_oss` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `url` varchar(200) DEFAULT NULL COMMENT 'URL地址',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文件上传';

-- ----------------------------
-- Records of sys_oss
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(100) DEFAULT NULL COMMENT '角色名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  `menu_id` bigint(20) DEFAULT NULL COMMENT '菜单ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='角色与菜单对应关系';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `salt` varchar(20) DEFAULT NULL COMMENT '盐',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `create_user_id` bigint(20) DEFAULT NULL COMMENT '创建者ID',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', 'admin', 'cdac762d0ba79875489f6a8b430fa8b5dfe0cdd81da38b80f02f33328af7fd4a', 'YzcmCZNvbXocrsz9dm8e', '478231309@qq.com', '15766190085', '1', '1', '2019-12-27 14:35:12');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户ID',
  `role_id` bigint(20) DEFAULT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='用户与角色对应关系';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_token
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_token`;
CREATE TABLE `sys_user_token` (
  `user_id` bigint(20) NOT NULL,
  `token` varchar(100) NOT NULL COMMENT 'token',
  `expire_time` datetime DEFAULT NULL COMMENT '过期时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `token` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户Token';

-- ----------------------------
-- Records of sys_user_token
-- ----------------------------
INSERT INTO `sys_user_token` VALUES ('1', '7f5150cd5c8ef19072edad0394b81c00', '2020-07-07 03:39:37', '2020-07-06 15:39:37');
INSERT INTO `sys_user_token` VALUES ('2', '7e7e0a979f74b294034a00febf191ea8', '2020-06-20 05:51:04', '2020-06-19 17:51:04');
INSERT INTO `sys_user_token` VALUES ('3', 'cc4dfadbf13b045b848aa7a52a87e139', '2020-06-20 05:52:19', '2020-06-19 17:52:19');
INSERT INTO `sys_user_token` VALUES ('4', 'b36d5fdbd5f0f1b8bc37065c41901d2f', '2020-06-20 05:51:45', '2020-06-19 17:51:45');

-- ----------------------------
-- Table structure for tb_goods
-- ----------------------------
DROP TABLE IF EXISTS `tb_goods`;
CREATE TABLE `tb_goods` (
  `goods_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL COMMENT '商品名',
  `intro` varchar(500) DEFAULT NULL COMMENT '介绍',
  `price` decimal(10,2) DEFAULT NULL COMMENT '价格',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`goods_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='商品管理';

-- ----------------------------
-- Records of tb_goods
-- ----------------------------
INSERT INTO `tb_goods` VALUES ('1', 'Nike鞋', '牛逼的鞋', '100.00', '1000');
INSERT INTO `tb_goods` VALUES ('2', '阿迪达斯鞋', '牛逼的鞋', '200.00', '100');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL COMMENT '用户名',
  `mobile` varchar(20) NOT NULL COMMENT '手机号',
  `password` varchar(64) DEFAULT NULL COMMENT '密码',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'mark', '15766190085', 'cdac762d0ba79875489f6a8b430fa8b5dfe0cdd81da38b80f02f33328af7fd4a', '2019-03-23 22:37:41');
